import requests
from bs4 import BeautifulSoup as bs

import os
import argparse

parser = argparse.ArgumentParser(
                    prog="Lyric'S-Crap",
                    description='Scrap script for download artist songs',
                    epilog='Enter the artist and song you want to get and it will be downloaded.')

parser.add_argument('-a', '--artist', type=str)
parser.add_argument('-s', '--song', type=str)
parser.add_argument('-p', '--prompt', type=bool)

args = parser.parse_args()

if args.artist:
    args.artist = "-".join(args.artist.split())
if args.song:
    args.song = "-".join(args.song.split())
if not args.song:
    args.song = ""

global sites
sites = {"cifraclub" : f"https://www.cifraclub.com.br/{args.artist}/{args.song}"}
print(sites)

class site:
    def __init__(self, url):
        self.url = url
        self.soup = bs(requests.get(url).content, 'html.parser')
        self.title = self.soup.title.text
   
    def get_song(self):
        try:
            song = self.soup.find("pre")
            print("downloading", self.title)
            return song.text
        except:
            print("no song specified")

if __name__ == '__main__':
    site = site(sites["cifraclub"])
    song = site.get_song()
    with open(site.title.replace(" ", "_").replace("/", "")+".txt", "w") as out:
        print("writing ", site.title)
        out.write(str(song))
